<?php
/**
 * Created by PhpStorm.
 * User: Estaban
 * Date: 15-12-2017
 * Time: 16:09
 */

namespace App\Service\Storage;


use Symfony\Component\HttpFoundation\File\UploadedFile;

interface StorageInterface {

	public function uploadFile(UploadedFile $uploadedFile, $privacy = 'public');

	public function deleteFile($key);

	public function listFiles($page = 1, $limit = 10);

	public function fileDetails($key);
}