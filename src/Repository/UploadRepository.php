<?php

namespace App\Repository;

use App\Entity\Upload;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UploadRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Upload::class);
    }

    
    public function findByUser($value,$offset,$limit)
    {
        $query =  $this->createQueryBuilder('u')
            ->where('u.user = :value')->setParameter('value', $value)
            ->orderBy('u.id', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery();
        $pagination = new Paginator($query);
        $pagination->setUseOutputWalkers(false);
        $query->setHint('knp_paginator.count', $pagination->count());
        $total = count($pagination);
        $items = $query->getResult();
        return array('items' => $items,'total' => $total);
    }
    
}
