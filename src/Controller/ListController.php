<?php
/**
 * Created by PhpStorm.
 * User: Estaban
 * Date: 15-12-2017
 * Time: 14:51
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller {

	/**
	 * @Route(path="/list/", name="list")
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function ListAction(Request $request){
		$storage = $this->get('app.storage.aws');
		$user = $this->getDoctrine()->getRepository('App:User')->find(1);
		$page = $request->get('page');
		$limit = $request->get('limit');
		if(empty($page)){
			$page = 0;
		}
		if(empty($limit)){
			$limit = 10;
		}
		$items = $storage->listFromDatabase($user,$page,$limit);
		return $this->render('list/list.html.twig', array(
			'items' => $items['items'],
			'total' => $items['total'],
		));
	}

	/**
	 * @Route(path="/detail/{id}/", name="listDetail")
	 * @param $id
	 *
	 * @return Response
	 */
	public function detailAction($id){
		$storage = $this->get('app.storage.aws');
		$key = $storage->detailsFromDatabase($id);
		$item = $storage->fileDetails($key->getFileLink());
		return $this->render('list/detail.html.twig', array(
			'item' => $item,
			'key' => $key,
		));
	}

    /**
     * @Route(path="/delete/{key}/")
     * @param $key
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
	public function deleteAction($key){
        $storage = $this->get('app.storage.aws');
        $storage->deleteFile($key);
        return $this->redirectToRoute('list');
    }
}