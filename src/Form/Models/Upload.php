<?php
/**
 * Created by PhpStorm.
 * User: Estaban
 * Date: 13-12-2017
 * Time: 14:39
 */

namespace App\Form\Models;


class Upload {

	/**
	 * @var string
	 */
	private $file;

    /**
     * @var string
     */
	private $name;

	/**
	 * @var \DateTime
	 */
	private $added;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $visibility;

	/**
	 * @var \DateTime
	 */
	private $expires_at;

	/**
	 * @return string
	 */
	public function getFile(): ?string {
		return $this->file;
	}

	/**
	 * @param string $file
	 */
	public function setFile( string $file ): void {
		$this->file = $file;
	}

	/**
	 * @return \DateTime
	 */
	public function getAdded(): \DateTime {
		return $this->added;
	}

	/**
	 * @param \DateTime $added
	 */
	public function setAdded( \DateTime $added ): void {
		$this->added = $added;
	}

	/**
	 * @return string
	 */
	public function getType(): ?string {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType( string $type ): void {
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getVisibility(): ?string {
		return $this->visibility;
	}

	/**
	 * @param string $visibility
	 */
	public function setVisibility( string $visibility ): void {
		$this->visibility = $visibility;
	}

	/**
	 * @return \DateTime
	 */
	public function getExpiresAt(): \DateTime {
		return $this->expires_at;
	}

	/**
	 * @param \DateTime $expires_at
	 */
	public function setExpiresAt( \DateTime $expires_at ): void {
		$this->expires_at = $expires_at;
	}

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

}